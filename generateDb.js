'use strict';
const config = require('config');
const MongoClient = require('mongodb').MongoClient;
const uuid = require('uuid/v4');
const dbName = config.get('database');

const products = [ "Red Currant Jelly", "Lemonade - Mandarin, 591 Ml", "Beans - Black Bean, Canned", "Wine - Chianti Classica Docg", "Soup - Beef, Base Mix", "Napkin White - Starched", "Lettuce - Escarole", "Basil - Pesto Sauce", "Cheese - Brie,danish", "Wine - Penfolds Koonuga Hill", "Squid Ink", "Tart Shells - Savory, 3", "Turkey - Ground. Lean", "Yoghurt Tubes", "Calypso - Strawberry Lemonade", "Galliano", "Chocolate Bar - Reese Pieces", "Broom - Angled", "Sauce - Vodka Blush", "Wine - White, Pelee Island", "Lettuce - Baby Salad Greens", "Vaccum Bag - 14x20", "Toamtoes 6x7 Select", "Wine - Prosecco Valdobiaddene", "Soup - Campbells, Chix Gumbo", "Lamb - Leg, Diced", "Muffin Mix - Morning Glory", "Wine - Casillero Del Diablo", "Red Currants", "Crackers - Water", "Wooden Mop Handle", "Extract - Almond", "Lid - Translucent, 3.5 And 6 Oz", "Liqueur Banana, Ramazzotti", "Wine - Jafflin Bourgongone", "Lentils - Green Le Puy", "Breadfruit", "Pepper - White, Ground", "Wine - Carmenere Casillero Del", "Tuna - Salad Premix", "Coffee - Egg Nog Capuccino", "Milk - Chocolate 250 Ml", "Hipnotiq Liquor", "Lettuce - Arugula", "Rice Wine - Aji Mirin", "Mushroom - Porcini, Dry", "Sauce - Soya, Light", "Soup - Campbells, Spinach Crm", "Bread - 10 Grain", "Wine - Vouvray Cuvee Domaine", "Bagels Poppyseed", "Bread - Focaccia Quarter", "Chicken - White Meat, No Tender", "Juice - Orange, 341 Ml", "Grand Marnier", "Coriander - Seed", "Vector Energy Bar", "Soup - Campbells, Minestrone", "Ecolab - Solid Fusion", "Bread - Multigrain", "Tea - Earl Grey", "Beef - Eye Of Round", "Bread - Multigrain Oval", "Bok Choy - Baby", "Lettuce - Green Leaf", "Savory", "Sauce - Roasted Red Pepper", "Prunes - Pitted", "Cheese - St. Paulin", "Bread Base - Gold Formel", "Lettuce - Red Leaf", "Mussels - Cultivated", "Cake - Mini Cheesecake", "Phyllo Dough", "Oil - Grapeseed Oil", "Garam Masala Powder", "Blueberries - Frozen", "Crackers - Soda / Saltins", "Nantucket - Carrot Orange", "Cookies Oatmeal Raisin", "Sambuca Cream", "Icecream - Dibs", "Seedlings - Clamshell", "Wine - Cotes Du Rhone Parallele", "Beans - Fava Fresh", "Sauce - Soya, Dark", "Juice - Apple, 341 Ml", "Slt - Individual Portions", "Table Cloth 54x54 Colour", "Capon - Breast, Wing On" ];
const names = [ "Jeramy", "Rosanna", "Luanna", "Teofila", "Leonardo", "Filiberto", "Ping", "Quinn", "Lynnette", "Edmond", "Norah", "Conchita", "Florencia", "Sherell", "Verdie", "Brendon", "Alfreda", "Tamekia", "Ligia", "Tonja", "Joannie", "Raleigh", "Eileen", "Yvone", "Ricky", "Latisha", "Ashely", "Eugene", "Misty", "Lesa", "Cayla", "Lexie", "Macy", "Emmett", "Cammy", "Marcelino", "Felton", "Trina", "Irina", "Celesta", "Sherron", "Christal", "Ozell", "Angeline", "Shirly", "Effie", "Nakita", "Sally", "Chantell", "Tiny" ];
const customerCount = 4;
const productRecords = 30;
const orderCount = 90;


(() => {
    MongoClient.connect(config.get("url"), { useNewUrlParser: true }, function (err, client) {
        if (err) throw err;
        const db = client.db(dbName);
        db.dropDatabase((error) => {
            if (error) throw error;
            start(db, () => {
                client.close();
            });
        });
    });
})();


function getRandomNumber (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomChar (length, addSpaces = false) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var charactersLength = characters.length;
    let breakAt = getRandomNumber(4, 10);
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        if (i % breakAt === 0 && addSpaces) {
            breakAt = getRandomNumber(4, 10);
            result += ' ';
        }
    }
    return result;
}


function getCustomer (index) {
    return {
        customerId: index,
        name: names[ index ],
    }
}

function getProduct (index) {
    return {
        productId: index,
        name: products[ index ],
        cost: getRandomNumber(1, 5000),
        uom: getRandomChar(4),
        description: getRandomChar(50, true)
    }
}

function getOrder (index) {
    const randomIndex = getRandomNumber(1, customerCount);
    return {
        orderId: index,
        createdBy: names[ randomIndex ],
        customerId: randomIndex,
        zip: getRandomNumber(1000, 9999),
        st: getRandomChar(2),
        picked: Math.random() > 0.5 ? "Yes" : "No",
        shipped: Math.random() > 0.5 ? "Yes" : "No",
        createdOn: new Date(new Date() - 60 * 60 * 1000 * getRandomNumber(1, 50)),
        lineItems: (
            () => {
                const items = getRandomNumber(1, productRecords);
                const array = Array(items).fill('');
                return array.map(() => ({
                    id: uuid(),
                    productId: getRandomNumber(1, productRecords),
                    quantity: getRandomNumber(1, 2000)
                }));
            })()
    };
}

function start (db, callback) {
    let response = 0;
    const cb = () => { response += 1; if (response === 3) callback(); }
    insertOrders(db, cb);
    insertProducts(db, cb);
    insertCustomers(db, cb);
}

function insertCustomers (db, cb) {
    const products = Array(customerCount).fill('').map((value, index) =>
        ({ insertOne: { "document": getCustomer(index + 1) } })
    );
    db.collection("customer").bulkWrite(products, (error) => {
        if (error) throw error;
        console.log('customer added successfully');
        cb();
    })
}

function insertProducts (db, cb) {
    const products = Array(productRecords).fill('').map((value, index) =>
        ({ insertOne: { "document": getProduct(index + 1) } })
    );
    db.collection("products").bulkWrite(products, (error) => {
        if (error) throw error;
        console.log('products added successfully');
        cb();
    })
}

function insertOrders (db, cb) {
    const orders = Array(orderCount).fill('').map((value, index) =>
        ({ insertOne: { "document": getOrder(index + 1) } })
    );
    db.collection("order").bulkWrite(orders, (error) => {
        if (error) throw error;
        console.log('orders added successfully');
        cb();
    })
}