'use strict';


const MongoClient = require('mongodb').MongoClient;
const config = require('config');
const dbName = config.get('database');

let clientReference;
let databaseReference;

function getDb (cb) {
    if (!clientReference || !databaseReference) {
        MongoClient.connect(config.get("url"), { useNewUrlParser: true }, function (err, client) {
            clientReference = client;
            databaseReference = client.db(dbName);
            cb(err, databaseReference);
        });
    }
    else {
        cb(null, databaseReference);
    }
}

function insertRecord (collectionName, record, callback) {
    databaseReference.collection(collectionName).insertOne(record, callback);
}

function deleteRecord (collectionName, query, callback) {
    databaseReference.collection(collectionName).deleteOne(query, callback);
}


function closeConnection () {
    if (clientReference) { clientReference.close(); }
}

module.exports = {
    deleteRecord,
    insertRecord,
    closeConnection,
    getDb
};