'use strict';
const router = require('koa-route');
const getProductsListing = require('../../interface/database/product').getProductsListing;
const response = require('../../utilities/response');


/**
 * @api {get} /lineItems Get LineItems master data
 * @apiName Get All LineItems
 * @apiGroup Product
 *
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/lineItems --request GET
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        {
 *            "error": null,
 *            "result": [
 *                {
 *                    "productId": 7,
 *                    "name": "Basil - Pesto Sauce",
 *                    "cost": 2755,
 *                    "uom": "PDUJ",
 *                    "description": "J HSZV FSE APT VZ PIQD WV WLRCAF VFC RSI LJBPI VEIAQEK HVPUNL M "
 *                }
 *            ]
 *        }     
 *
 */
async function getProductsList (ctx) {
    let data;
    try {
        data = await getProductsListing(ctx.request.query);
    } catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.get('/lineItems', getProductsList);