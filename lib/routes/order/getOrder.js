'use strict';
const router = require('koa-route');
const getOrderById = require('../../interface/database/order').getOrderById;
const errors = require('../../utilities/error');
const response = require('../../utilities/response');

function validateParams (orderId) {
    if (!orderId || isNaN(orderId)) {
        throw new errors.InvalidRequestParameter('Invalid Order Id');
    }
}

/**
 * @api {get} /orders/:orderId?offset Get all details of an order by Id
 * 
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/orders/1?offset=0 --request GET
 * 
 * @apiName Get order detail by id
 * @apiGroup Order
 *
 * @apiParam {Number} orderId OrderId to get.
 * @apiParam {Number} offset Offset for lineItems.
 *
 * @apiSuccessExample Order Found
 *     GET http://localhost:5000/orders/1
 *     HTTP/1.1 200 OK
 *  {
 *      error: null,
 *      result: {
 *              "orderId":1,
 *              "createdBy":"Smith",
 *              "createdOn":"2019-05-22T12:01:30.367Z",
 *              "shipped":"Yes",
 *              "picked":"Yes",
 *              "st":"AZ",
 *              "lineItems":[
 *                  {"id":"ec73d802-02ec-41ed-8e79-80943448683c","quantity":2000,"name":"nails"},
 *                  {"id":"fdb61e29-0fa6-4cb5-9ce0-eae7e3ffe2d2","quantity":100,"name":"hammer"}
 *              ]   
 *      }
 *  }
 * 
 *  @apiSuccessExample Offset is high (lineItems offset)
 *     GET http://localhost:5000/orders/1?offset=10000
 *     HTTP/1.1 200 OK
 *  {
 *      error: null,
 *      result: {
 *              "orderId":1,
 *              "createdBy":"Smith",
 *              "createdOn":"2019-05-22T12:01:30.367Z",
 *              "shipped":"Yes",
 *              "picked":"Yes",
 *              "st":"AZ",
 *              "lineItems":[]   
 *      }
 *  }
 * 
 *  @apiSuccessExample Order Not Found
 *     GET http://localhost:5000/orders/10000
 *     HTTP/1.1 200 OK
 *  {
 *      error: {
 *          name: 'DatabaseQueryEror',
 *          "message": "No order found"
 *      },
 *      result: {}
 *  }
 */
async function getOrder (ctx, id) {
    let data;
    let offset = 0;
    if (ctx.query.offset && !isNaN(ctx.query.offset)) {
        offset = parseInt(ctx.query.offset);
    }
    try {
        validateParams(id);
        const order = await getOrderById(parseInt(id), offset);
        data = order.length ?
            order[ 0 ] :
            new errors.DatabaseQueryError({ message: 'No order found' });
    }
    catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.get('/orders/:id', getOrder);