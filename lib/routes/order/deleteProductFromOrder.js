'use strict';
const router = require('koa-route');
const deleteProductFromOrder = require('../../interface/database/order').deleteProductFromOrder;
const response = require('../../utilities/response');
const isUUID = require('../../utilities/isUUID');

function validateParams (orderId, lineItemId) {
    if (isNaN(orderId) || !isUUID(lineItemId)) {
        throw new errors.InvalidRequestParameter('Invalid parameters');
    }
}


/**
 * @api {delete} /orders/:orderId/:productId Remove product from order's lineItems
 * @apiName Remove product from an order by id
 * @apiGroup Order
 *
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/orders/1/ec73d802-02ec-41ed-8e79-80943448683c --request DELETE
 * 
 * 
 * @apiParam {Number} orderId orderId to update.
 * @apiParam {Number} lineItemId lineItem to remove.
 *
 * 
 *  @apiSuccessExample Order Deleted (lineItem existed in that order)
 *     DELETE http://localhost:5000/orders/1/ec73d802-02ec-41ed-8e79-80943448683c
 *     HTTP/1.1 200 OK
 *  {
 *      error: null,
 *      result: {
 *          "error": null,
 *          "result": {
 *               "n": 1,
 *               "nModified": 1,
 *               "ok": 1
 *           }
 *       }
 *  }
 * 
 *  @apiSuccessExample Order Deleted (lineItem didn't existed)
 *     DELETE http://localhost:5000/orders/1/ec73d802-02ec-41ed-8e79-80943448683c
 *     HTTP/1.1 200 OK
 *  {
 *      error: null,
 *      result: {
 *          "error": null,
 *          "result": {
 *               "n": 1,
 *               "nModified": 0,
 *               "ok": 1}
 *       }
 *  }
 * 
 */
async function deleteProductFromOrderRoute (ctx, orderId, lineItemId) {
    let data;
    try {
        validateParams(orderId, lineItemId);
        data = await deleteProductFromOrder(
            parseInt(orderId),
            lineItemId
        );
    } catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.delete('/orders/:orderId/:lineItemId', deleteProductFromOrderRoute);