'use strict';
const router = require('koa-route');
const getAllOrders = require('../../interface/database/order').getAllOrders;
const response = require('../../utilities/response');

/**
 * @api {get} /orders?customerId=1&sortBy=orderId Request Order Listing
 * @apiName Get Order List
 * @apiGroup Order
 *
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/orders?'offset=0&sortBy=orderId&customerId=1' --request GET
 * 
 * @apiParam {Number} customerId customerID to search for.
 * @apiParam {String} sortBy An enum value of createdOn or orderId
 * @apiParam {Number} offset offset
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK            
 *        {
 *            "error": null,
 *            "result": {
 *                "customer": {
 *                    "customerId": 1,
 *                    "name": "Rosanna"
 *                },
 *                "orders": [
 *                    {
 *                        "orderId": 1,
 *                        "createdBy": "Rosanna",
 *                        "zip": 4030,
 *                        "st": "JZ",
 *                        "picked": "Yes",
 *                        "shipped": "No",
 *                        "createdOn": "2019-05-24T03:54:52.832Z"
 *                    },
 *                    {
 *                        "orderId": 2,
 *                        "createdBy": "Rosanna",
 *                        "zip": 5419,
 *                        "st": "ID",
 *                        "picked": "No",
 *                        "shipped": "No",
 *                        "createdOn": "2019-05-24T10:54:52.834Z"
 *                    }
 *                ]
 *            }
 *        }
 *
 */
async function getOrderList (ctx) {
    let data;
    try {
        const query = ctx.request.query;
        const search = {};
        let sortQuery = { orderId: 1 };
        const limit = 20;
        let offset = 0;

        if (query.sortBy === 'createdOn') {
            sortQuery = { createdOn: -1 };
        }

        if (query.offset && !isNaN(query.offset)) {
            offset = parseInt(query.offset);
        };

        if (query.customerId && !isNaN(query.customerId)) {
            search.customerId = parseInt(query.customerId);
            data = await getAllOrders(search, sortQuery, limit, offset);
        }
        else {
            data = {
                customer: {},
                orders: []
            };
        }

    } catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.get('/orders', getOrderList);