'use strict';
const router = require('koa-route');
const addProductInOrder = require('../../interface/database/order').addProductInOrder;
const getProductById = require('../../interface/database/product').getProductById;
const response = require('../../utilities/response');

function validateParams (orderId, productId) {
    if (isNaN(orderId) || isNaN(productId)) {
        throw new errors.InvalidRequestParameter('Invalid parameters');
    }
}


/**
 * @api {put} /orders/:orderId Add product in order's lineItems
 * 
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/orders/1 --request PUT --data '{"productId": 1}' -H "Content-Type: application/json"
 * 
 * @apiName Add product in an order
 * @apiGroup Order
 *
 * @apiParam {Number} orderId orderId to update.
 * @apiParam {Number} productId product to add.
 * 
 * @apiSuccessExample Product added
 *     PUT http://localhost:5000/orders/1 
 *     HTTP/1.1 200 OK
 *      {
 *       "error": null,
 *       "result": {
 *           "id": "ec73d802-02ec-41ed-8e79-80943448683c",
 *           "productId": 1,
 *           "quantity": 100,
 *           "name": "Lemonade - Mandarin, 591 Ml",
 *           "cost": 2715,
 *           "uom": "ZGCZ",
 *           "description": "K KGFFUGRH CR YHMJ IIAITFN HAONDE MPPWT CDSS DPZNKL IVYZEXS "
 *       }
 *      }
 *
 */
async function addProductInOrderRoute (ctx, orderId) {
    let data;
    try {
        validateParams(orderId, ctx.request.body.productId);
        const lineItemData = await addProductInOrder(
            parseInt(orderId),
            parseInt(ctx.request.body.productId)
        );
        const productData = await getProductById({ productId: ctx.request.body.productId });
        data = Object.assign(lineItemData, productData);
    } catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.put('/orders/:orderId', addProductInOrderRoute);