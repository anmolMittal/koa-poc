'use strict';
const router = require('koa-route');
const updateProductInOrder = require('../../interface/database/order').updateProductInOrder;
const errors = require('../../utilities/error');
const response = require('../../utilities/response');
const isUUID = require('../../utilities/isUUID');

function validateParams (orderId, lineItemId, quantity) {
    if (isNaN(orderId) || !isUUID(lineItemId) || isNaN(quantity)) {
        throw new errors.InvalidRequestParameter('Invalid parameters');
    }
}


/**
 * @api {put} /orders/:orderId/:productId Update product's quantity in the order
 * 
 * @apiExample {curl} Usage
 *  curl http://localhost:5000/orders/1/fdb61e29-0fa6-4cb5-9ce0-eae7e3ffe2d2 --request PUT --data '{"quantity": 10}' -H "Content-Type: application/json"
 * 
 * @apiName Update product's quantity in the order
 * @apiGroup Order
 *
 * @apiParam {Number} orderId orderId to update.
 * @apiParam {Number} productId product to update.
 * @apiparam {Number} quantity quantity of product to set.
 * 
 *  
 *  @apiSuccessExample Product didn't existed
 *     GET http://localhost:5000/orders/1/2000
 *     HTTP/1.1 200 OK
 *      {
 *        error: null,
 *        result: {
 *                  "n": 0,
 *                  "nModified": 0,
 *                  "ok": 1
 *               }
 *       }
 * 
 *  @apiSuccessExample Product existed
 *     GET http://localhost:5000/orders/1/2
 *     HTTP/1.1 200 OK
 *      {
 *        error: null,
 *        result: {
 *                  "n": 1,
 *                  "nModified": 1,
 *                  "ok": 1
 *               }
 *       }
 * 
 * @apiSuccessExample Invalid Parameters
 *     GET http://localhost:5000/orders/1sfdsd/2
 *     HTTP/1.1 200 OK
 *     {
 *          "error": {
 *              "name": "InvalidRequestParameter",
 *              "message": "Invalid parameters"
 *           },
 *           "result": {}
 *      }
 * 
 */
async function updateProductInOrderRoute (ctx, orderId, lineItemId) {
    let data;
    try {
        validateParams(orderId, lineItemId, ctx.request.body.quantity);
        data = await updateProductInOrder(
            parseInt(orderId),
            lineItemId,
            ctx.request.body.quantity
        );
    } catch (error) {
        data = error;
    }
    finally {
        ctx.body = response.generateRepsonse(data);
    }
}

module.exports = router.put('/orders/:orderId/:lineItemId', updateProductInOrderRoute);