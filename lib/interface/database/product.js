'use strict';

const db = require('../../init/database').getDatabase;
const errors = require('../../utilities/error');
const entityName = 'products';

const ProductInterface = {
    getProductsListing,
    getProductById
};

module.exports = ProductInterface;


function getProductById (query) {
    return new Promise((resolve, reject) => {
        db().collection(entityName).aggregate([
            { $match: query },
            { $addFields: { quantity: 100 } },
            { $project: { '_id': 0 } }
        ]).toArray((error, result) => {
            if (error) return reject(new errors.DatabaseQueryError(error));
            resolve(result[ 0 ]);
        });
    });
}

function getProductsListing (query) {
    let filter = {};
    if (query.name) {
        filter.name = { $regex: query.name, $options: "i" };
    }
    return new Promise((resolve, reject) => {
        db().collection(entityName)
            .find(filter)
            .project({ _id: 0 })
            .sort({ "name": 1 })
            .toArray((error, result) => {
                if (error) return reject(new errors.DatabaseQueryError(error));
                resolve(result);
            });
    });

}