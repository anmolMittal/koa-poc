'use strict';
const db = require('../../init/database').getDatabase;
const errors = require('../../utilities/error');
const uuid = require('uuid/v4');
const entityName = 'order';
const customerEntity = 'customer';


const OrderInterface = {
    getOrderById,
    getAllOrders,
    updateProductInOrder,
    addProductInOrder,
    deleteProductFromOrder
}

module.exports = OrderInterface;

function getOrderById (orderId, offset) {
    const pipeline = [
        { $match: { orderId } },
        { $unwind: { "path": "$lineItems", "preserveNullAndEmptyArrays": true } },
        {
            $lookup: {
                from: 'products',
                localField: 'lineItems.productId',
                foreignField: 'productId',
                as: 'lineItems.product'
            }
        },
        {
            $unwind: {
                "path": "$lineItems.product",
                "preserveNullAndEmptyArrays": true
            }
        },
        { $sort: { "lineItems.product.name": 1 } },
        {
            $group: {
                "_id": "$_id",
                "orderId": { $first: "$orderId" },
                "createdBy": { $first: "$createdBy" },
                "createdOn": { $first: "$createdOn" },
                "shipped": { $first: "$shipped" },
                "picked": { $first: "$picked" },
                "st": { $first: "$st" },
                "zip": { $first: "$zip" },
                "lineItems": {
                    "$push": {
                        "$cond": [
                            { "$gt": [ "$lineItems.id", 0 ] },
                            "$lineItems",
                            null
                        ]
                    }
                }
            }
        },
        {
            $project: {
                "lineItems": {
                    $slice: [ {
                        $map: {
                            input: { $setDifference: [ "$lineItems", [ null ] ] },
                            as: "lineItems",
                            in: {
                                $mergeObjects: [
                                    { id: "$$lineItems.id" },
                                    { quantity: "$$lineItems.quantity" },
                                    { cost: "$$lineItems.product.cost" },
                                    { uom: "$$lineItems.product.uom" },
                                    { description: "$$lineItems.product.description" },
                                    { name: "$$lineItems.product.name" }
                                ]
                            }
                        },
                    },
                        offset,
                        20
                    ]
                },
                "orderId": 1,
                "createdBy": 1,
                "createdOn": 1,
                "shipped": 1,
                "picked": 1,
                "st": 1,
                "_id": 0
            },
        }
    ];
    return new Promise((resolve, reject) => {
        db().collection(entityName).aggregate(pipeline).toArray((error, data) => {
            if (error) {
                return reject(new errors.DatabaseQueryError(error));
            }
            resolve(data);
        });
    });
}

function addProductInOrder (orderId, productId, quantity = 100) {
    const query = {
        orderId
    };
    const lineItemData = { id: uuid(), productId, quantity }
    const updateQuery = {
        $push: { "lineItems": lineItemData }
    }
    return new Promise((resolve, reject) => {
        db().collection(entityName).updateOne(query, updateQuery, (error, doc) => {
            if (error) return reject(new errors.DatabaseQueryError(error));
            resolve(lineItemData);
        });
    });
}

function updateProductInOrder (orderId, lineItemId, quantity) {
    const query = {
        "orderId": orderId,
        "lineItems.id": lineItemId
    };
    const updateQuery = { $set: { "lineItems.$.quantity": quantity } };
    return new Promise((resolve, reject) => {
        db().collection(entityName).updateOne(query, updateQuery, (error, doc) => {
            if (error) return reject(new errors.DatabaseQueryError(error));
            resolve(doc.result);
        });
    });
}

function deleteProductFromOrder (orderId, lineItemId) {
    const query = {
        orderId
    };

    const updateQuery = {
        $pull: { "lineItems": { id: lineItemId } }
    };

    return new Promise((resolve, reject) => {
        db().collection(entityName).updateOne(query, updateQuery, (error, doc) => {
            if (error) return reject(new errors.DatabaseQueryError(error));
            resolve(doc.result);
        });
    });
}

function getAllOrders (search, sortQuery, limit, offset) {
    const operations = [];

    operations.push(new Promise((resolve, reject) => {
        db().collection(customerEntity)
            .findOne(search, { projection: { _id: 0 } }, (error, result) => {
                if (error) {
                    reject(new errors.DatabaseQueryError(error));
                }
                resolve(result);
            });
    }));

    operations.push(new Promise((resolve, reject) => {
        db().collection(entityName)
            .find(search)
            .skip(offset)
            .sort(sortQuery)
            .limit(limit)
            .project({ lineItems: 0, _id: 0, customerId: 0 })
            .toArray((err, result) => {
                if (err) return reject(new errors.DatabaseQueryError(err));
                resolve(result);
            });
    }));

    return Promise.all(operations).then((results) => {
        return {
            customer: results[ 0 ] || {},
            orders: results[ 1 ]
        }
    });
}