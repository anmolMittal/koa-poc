'use strict';

function InvalidRequestParameter (message) {
    this.name = 'InvalidRequestParameter';
    this.message = message;
    this.type = 'error';
}

function DatabaseQueryError (error) {
    this.name = 'DatabaseQueryError';
    this.message = error.message;
    this.type = 'error';
}

module.exports = { InvalidRequestParameter, DatabaseQueryError };