'use strict';


function generateRepsonse (data) {
    if (data.type === 'error') {
        return {
            error: {
                name: data.name,
                message: data.message
            },
            result: {}
        }
    }
    else {
        return {
            error: null,
            result: data
        }
    }
}

module.exports = { generateRepsonse };