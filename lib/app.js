'use strict';

const Koa = require('koa');
const cors = require('@koa/cors');
const config = require('config');
const initializeDatabase = require('./init/database').initializeDatabase;
var bodyParser = require('koa-bodyparser');

const app = new Koa();

async function start () {
    try {
        await initializeDatabase();
    } catch (error) {
        console.log('Unable to initialize database :', error);
    }
    return app.listen(config.get('port'), () => {
        console.log('server listening on port', config.get('port'));
    });
};

app.use(cors());
app.use(bodyParser());
app.use(require('./routes/order/updateProductInOrder'));
app.use(require('./routes/order/addProductInOrder'));
app.use(require('./routes/order/getOrder'));
app.use(require('./routes/order/getOrderList'));
app.use(require('./routes/order/deleteProductFromOrder'));
app.use(require('./routes/product/getProductList'));


app.use(ctx => {
    ctx.body = {
        message: 'Invalid route'
    };
});

module.exports = start;