'use strict';

const config = require('config');
const MongoClient = require('mongodb').MongoClient;

const dbName = config.get('database');
let database;

async function getClient () {
    return new Promise((resolve) => {
        MongoClient.connect(config.get("url"), { useNewUrlParser: true }, function (err, client) {
            if (err) throw err;

            database = client.db(dbName);
            createIndexes(database, [
                { collection: 'order', key: 'orderId' },
                { collection: 'products', key: 'productId' },
                { collection: 'customer', key: 'customerId' }
            ], (err) => {
                if (err) {
                    throw err;
                }
                console.log('connected to database');
                return resolve(database);
            });
        });
    });
}

function createIndexes (database, records, callback) {
    let responses = 0;
    records.forEach((record) => {
        database.collection(record.collection).createIndex(record.key, { unique: true }, (error, result) => {
            responses += 1;
            if (responses === 3) {
                callback(error);
            }
        });


    });
}

exports.initializeDatabase = getClient;
exports.getDatabase = () => database;
