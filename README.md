# koa-poc
POC of koa

# Getting Started
Run `npm run setup` to install/setup all dependencies for the project and start the server on default port `5000`

# Specific commands
1. Run `npm run create:db` to create a fresh copy of data in database provided in config
2. Run `npm run create:doc` to create API docs at root level
3. Run `npm start` to start server on the port
4. Run `npm test` to run test cases for the project (default port 7000)
5. Run `npm run test:integration` to start integration tests (mongo server should be started)
6. Run `npm run test:unit` to start unit tests