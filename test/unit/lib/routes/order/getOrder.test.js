'use strict';

const sinon = require('sinon');
const expect = require('chai').expect;
const mockery = require('mockery');

describe('get order details unit test case', () => {
    let getOrderRoute;
    let koaRoute;
    let getOrderById;
    let errors;
    let response;

    beforeEach(() => {
        mockery.enable({ useCleanCache: true });
        mockery.registerMock('koa-route', koaRoute = {
            get: sinon.stub()
        });
        mockery.registerMock(
            '../../interface/database/order',
            { getOrderById: getOrderById = sinon.stub() }
        );
        mockery.registerMock('../../utilities/error', errors = {
            DatabaseQueryError: sinon.stub(),
            InvalidRequestParameter: sinon.stub()
        });
        mockery.registerMock('../../utilities/response', response = {
            generateRepsonse: sinon.stub()
        });
        mockery.registerAllowable('../../../../../lib/routes/order/getOrder');

        koaRoute.get.withArgs('/orders/:id', sinon.match.any).returns('ok');
        getOrderRoute = require('../../../../../lib/routes/order/getOrder');
    });

    afterEach(() => {
        mockery.deregisterAll();
        mockery.disable();
    });

    it('should return value returned by get method of koa-route on receiving expected parameters', () => {
        expect(getOrderRoute).to.equal('ok');
    });

    it('should set ctx body to InvalidRequestParameter when orderId is not numeric', () => {
        const fn = koaRoute.get.args[ 0 ][ 1 ];
        const ctx = { query: {} };
        errors.InvalidRequestParameter
            .withArgs('Invalid Order Id')
            .returns({ name: 'test error' });
        response.generateRepsonse.returnsArg(0);
        return fn(ctx, 'invalidId').then(() => {
            expect(ctx.body).to.eql({
                name: 'test error'
            });
        });
    });

    it('should return expected array return by database interface function', () => {
        const fn = koaRoute.get.args[ 0 ][ 1 ];
        const ctx = { query: {} };
        getOrderById.withArgs(12, 0).resolves([ { name: 'first record' } ]);
        response.generateRepsonse.withArgs({ name: 'first record' }).returnsArg(0);
        return fn(ctx, '12').then(() => {
            expect(ctx.body).to.eql({
                name: 'first record'
            });
        });
    });

    it('should return expected message when records are empty', () => {
        const fn = koaRoute.get.args[ 0 ][ 1 ];
        const ctx = { query: {} };
        getOrderById.withArgs(12, 0).resolves([]);
        errors.DatabaseQueryError
            .withArgs({ message: 'No order found' })
            .returns({ name: 'no record message' });
        response.generateRepsonse.withArgs({ name: 'no record message' }).returnsArg(0);
        return fn(ctx, '12').then(() => {
            expect(ctx.body).to.eql({ name: 'no record message' });
        });
    });

    it('should set ctx.body to whatever response.generateResponse returns', () => {
        const fn = koaRoute.get.args[ 0 ][ 1 ];
        const ctx = { query: {} };
        getOrderById.withArgs(12, 0).resolves([]);
        errors.DatabaseQueryError
            .withArgs({ message: 'No order found' })
            .returns({ name: 'no record message' });
        response.generateRepsonse.returns({ name: 'test record' });
        return fn(ctx, '12').then(() => {
            expect(ctx.body).to.eql({ name: 'test record' });
        });
    });

    it('should handle if getOrderById interface function throws error', () => {
        const fn = koaRoute.get.args[ 0 ][ 1 ];
        const ctx = { query: {} };
        getOrderById.withArgs(12, 0).throws('UnexpectedError');
        errors.DatabaseQueryError
            .withArgs({ message: 'No order found' })
            .returns({ name: 'no record message' });
        response.generateRepsonse.returnsArg(0);
        return fn(ctx, '12').then(() => {
            expect(ctx.body.name).to.eql('UnexpectedError');
        });
    });
});