'use strict';

const server = require('../../../lib/app');
const { getDb, closeConnection } = require('../../../test-utilities/database/operations');

before('should start the server', (done) => {
    server().then((app) => {
        global.app = app;
        done();
    });
});

before('should establish database connection', (done) => {
    getDb(() => {
        done();
    })
});

after('close the server', (done) => {
    global.app.close();
    done();
});

after('close database connection', () => {
    closeConnection();
});
