'use strict';
const expect = require('chai').expect;
const { insertRecord, deleteRecord } = require('../../../../../test-utilities/database/operations');

const request = require('supertest');

describe('update product in order', () => {
    let date;
    let lineItemId;

    before('should create an order', (done) => {
        date = new Date();
        const order = {
            orderId: 1,
            createdBy: "test user",
            customerId: 1,
            zip: 4000,
            st: 'ZT',
            picked: "No",
            shipped: "No",
            createdOn: date,
            lineItems: []
        };
        insertRecord('order', order, done);
    });

    before('should create a product', (done) => {
        const product = {
            "productId": 2,
            "name": "Lemonade - Mandarin, 591 Ml",
            "cost": 980,
            "uom": "KSYU",
            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
        };
        insertRecord('products', product, done);
    });

    after('should remove that order', (done) => {
        deleteRecord('order', { orderId: 1 }, done);
    });

    after('should remove the product', (done) => {
        deleteRecord('products', { productId: 2 }, done);
    });

    it('should have empty lineItems array', () => {
        return request(global.app).get('/orders/1').then((response) => {
            expect(response.body).to.eql({
                error: null,
                result: {
                    orderId: 1,
                    createdBy: 'test user',
                    createdOn: date.toISOString(),
                    shipped: 'No',
                    picked: 'No',
                    st: 'ZT',
                    lineItems: []
                }
            });
        });
    });

    it('should add that product in the order', () => {
        return request(global.app).put('/orders/1').send({ productId: 2 }).
            then((response) => {
                const body = response.body;
                lineItemId = body.result.id;
                expect(body.error).to.be.null;
                expect(body.result.id).not.to.be.undefined;
                expect([
                    body.result.productId,
                    body.result.quantity,
                    body.result.name,
                    body.result.cost,
                    body.result.uom,
                    body.result.description
                ]).to.eql([ 2,
                    100,
                    "Lemonade - Mandarin, 591 Ml",
                    980,
                    "KSYU",
                    "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
                ]);
            });
    });

    it('should have expected (populated) lineItems array', () => {
        return request(global.app).get('/orders/1').then((response) => {
            expect(response.body).to.eql({
                error: null,
                result: {
                    orderId: 1,
                    createdBy: 'test user',
                    createdOn: date.toISOString(),
                    shipped: 'No',
                    picked: 'No',
                    st: 'ZT',
                    lineItems: [ {
                        "name": "Lemonade - Mandarin, 591 Ml",
                        "cost": 980,
                        "uom": "KSYU",
                        "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF",
                        "id": lineItemId,
                        "quantity": 100
                    } ]
                }
            });
        });
    });
});