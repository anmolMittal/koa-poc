'use strict';
const uuid = require('uuid/v4');
const expect = require('chai').expect;
const { insertRecord, deleteRecord } = require('../../../../../test-utilities/database/operations');
const request = require('supertest');

describe('get order', () => {
    let date;
    let lineItemId1, lineItemId2;

    before('should create an order', (done) => {
        date = new Date();
        lineItemId1 = uuid();
        lineItemId2 = uuid();
        const order = {
            orderId: 1,
            createdBy: "test user",
            customerId: 1,
            zip: 4000,
            st: 'ZT',
            picked: "No",
            shipped: "No",
            createdOn: date,
            lineItems: [
                { id: lineItemId1, productId: 2, quantity: 203 },
                { id: lineItemId2, productId: 3, quantity: 400 }
            ]
        };
        insertRecord('order', order, done);
    });

    before('should create a product', (done) => {
        const product = {
            "productId": 2,
            "name": "Lemonade - Mandarin, 591 Ml",
            "cost": 980,
            "uom": "KSYU",
            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
        };
        insertRecord('products', product, done);
    });

    before('should create second product', (done) => {
        const product = {
            "productId": 3,
            "name": "Beans - Black Bean, Canned",
            "cost": 290,
            "uom": "LTU",
            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
        };
        insertRecord('products', product, done);
    });

    after('should remove that order', (done) => {
        deleteRecord('order', { orderId: 1 }, done);
    });

    after('should remove the product 1', (done) => {
        deleteRecord('products', { productId: 2 }, done);
    });

    after('should remove the product 2', (done) => {
        deleteRecord('products', { productId: 3 }, done);
    });

    it('should get the expected order details (with lineItems sorted by name)', (done) => {
        request(global.app).get('/orders/1').then((response) => {
            expect(response.body).to.eql({
                error: null,
                result: {
                    orderId: 1,
                    createdBy: 'test user',
                    createdOn: date.toISOString(),
                    shipped: 'No',
                    picked: 'No',
                    st: 'ZT',
                    lineItems: [
                        {
                            "id": lineItemId2,
                            "quantity": 400,
                            "name": "Beans - Black Bean, Canned",
                            "cost": 290,
                            "uom": "LTU",
                            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
                        },
                        {
                            "id": lineItemId1,
                            "quantity": 203,
                            "name": "Lemonade - Mandarin, 591 Ml",
                            "cost": 980,
                            "uom": "KSYU",
                            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
                        } ]
                }
            });
            done();
        });

    });

    it('should get the expected order when offset is passed', (done) => {
        request(global.app).get('/orders/1?offset=1').then((response) => {
            expect(response.body).to.eql({
                error: null,
                result: {
                    orderId: 1,
                    createdBy: 'test user',
                    createdOn: date.toISOString(),
                    shipped: 'No',
                    picked: 'No',
                    st: 'ZT',
                    lineItems: [
                        {
                            "id": lineItemId1,
                            "quantity": 203,
                            "name": "Lemonade - Mandarin, 591 Ml",
                            "cost": 980,
                            "uom": "KSYU",
                            "description": "M BVHJW R UI W I OE EGZE RQITMFTP EVZG VXNK FZRN BFTTLV FUU CHRF"
                        }
                    ]
                }
            });
            done();
        });
    });

    it('should return empty lineItems array when offset is more than lineItems count', (done) => {
        request(global.app).get('/orders/1?offset=3').then((response) => {
            expect(response.body).to.eql({
                error: null,
                result: {
                    orderId: 1,
                    createdBy: 'test user',
                    createdOn: date.toISOString(),
                    shipped: 'No',
                    picked: 'No',
                    st: 'ZT',
                    lineItems: []
                }
            });
            done();
        });
    });

    it('should return expected message when orderId does not exists', (done) => {
        request(global.app).get('/orders/10').then((response) => {
            expect(response.body).to.eql({
                error: { name: 'DatabaseQueryError', message: 'No order found' },
                result: {}
            });
            done();
        });
    });

    it('should return expected message when orderId is invalid', (done) => {
        request(global.app).get('/orders/sfds').then((response) => {
            expect(response.body).to.eql({
                error: { name: 'InvalidRequestParameter', message: 'Invalid Order Id' },
                result: {}
            });
            done();
        });
    });
});